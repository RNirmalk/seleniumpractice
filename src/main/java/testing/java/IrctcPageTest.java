package testing.java;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcPageTest {

	public static void main(String[] args) throws InterruptedException {
		
		//C:\\NirDocs\\Selenium\\
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\NirDocs\\Selenium\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().timeouts().implicitlyWait(30000, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//Thread.sleep(7000);
	
		System.out.println(driver.findElementByName("userRegistrationForm:userName").isEnabled());
		driver.findElementByName("userRegistrationForm:userName").sendKeys("Test9301");
		driver.findElementById("userRegistrationForm:password").sendKeys("Wertclo2");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Wertclo2");
		//SecurityQuestion
		WebElement Sec= driver.findElementById("userRegistrationForm:securityQ");
		
				Select sel1 = new Select(Sec);
		sel1.selectByVisibleText("What is your pet name?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("TestPet");
		//FName
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Test");
		//Gender
		driver.findElementByXPath("//input[@id='userRegistrationForm:gender:0']").click();
		//MaritalStatus
		driver.findElementByXPath("//input[@id='userRegistrationForm:maritalStatus:1']").click();
		
		//DOB
		
		Select s2 = new Select(driver.findElementByName("userRegistrationForm:dobDay"));
		s2.selectByVisibleText("18");
		
		Select s3 = new Select(driver.findElementByName("userRegistrationForm:dateOfBirth"));
		s3.selectByVisibleText("1993");
		//Occupation
		
		Select s4 = new Select(driver.findElementByName("userRegistrationForm:occupation"));
		s4.selectByVisibleText("Private");
		
		//country
		Select s5 = new Select(driver.findElementByName("userRegistrationForm:countries"));
		s5.selectByVisibleText("India");
		//List<WebElement> allSelectedOptions = s5.getAllSelectedOptions();
		//allSelectedOptions.contains(arg0)
		
		//Email
		
		
		driver.findElementById("userRegistrationForm:email").sendKeys("abc@gmail.com");
		
		//Phone
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9884629282");
		//Nationality
		Select s6= new Select(driver.findElementById("userRegistrationForm:nationalityId"));
		s6.selectByVisibleText("India");
		
		//FlatNo
		driver.findElementById("userRegistrationForm:address").sendKeys("Porur");
		
		//pin code*	
		
		 driver.findElementById("userRegistrationForm:pincode").sendKeys("600116");
		 
		 driver.findElementById("userRegistrationForm:pincode").sendKeys(Keys.TAB);
		 //Keys.TAB;
		//state*	
		 
		 System.out.println(driver.findElementById("userRegistrationForm:statesName").isEnabled());
		 //Thread.sleep(5000);
		String st = driver.findElementById("userRegistrationForm:statesName").getText();
		System.out.println(st);
		Thread.sleep(5000);
		 
		//City/Town*
		Select s7= new Select(driver.findElementByXPath("//Select[@id='userRegistrationForm:cityName'][1]"));
	//	Thread.sleep(5000);
		s7.selectByValue("Kanchipuram");
		driver.findElementByXPath("//Select[@id='userRegistrationForm:cityName'][1]").sendKeys(Keys.TAB);
		Thread.sleep(5000);
		//Post Office*	
		Select s8= new Select(driver.findElementByXPath("//Select[@id='userRegistrationForm:postofficeName'][1]"));
		s8.selectByVisibleText("Porur S.O");
	
		
		//phone *
		
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("9884625252");
		
		//RadioButton
		
		//driver.findElementByXPath("//input[@id='userRegistrationForm:resAndOff:1']").click();
		
		Thread.sleep(50000);
		
		driver.quit();
		
		/*
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(25000, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("Test");
		*/
	}

}
