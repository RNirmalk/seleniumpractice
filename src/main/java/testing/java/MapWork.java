package testing.java;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MapWork {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		char[] ch = str.toCharArray();

		Map<Character, Integer> obj = new LinkedHashMap<>();
		
		/*for (int i = 0; i < ch.length; i++) {
			obj.put(ch[i], 1);
		}*/
		
		
		
		for (char c : ch) {
			if(obj.containsKey(c))
				obj.put(c, obj.get(c)+1);
			else
			obj.put(c,1);
		}
		
		for (char c : ch) {
			System.out.println(obj.get(c));	
		}
/*for (Map.Entry<Character, Integer> c : obj.entrySet()) {
	
	System.out.println(c.getKey() + " " + c.getValue());
}*/
		
		
	}

}
