package testcases.leaftaps;

import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLead extends ProjectMethods{
	

@BeforeTest
public void initializeCredentials() {
	testcaseName = "CreateLead";
	testDec = "Create a new Lead in leaftaps";
	author = "Nirmal";
	category = "Smoke";
	sheetname="Test01";
}


@Test(dataProvider="fetchData")
public void createLead(String CM,String FN,String LN) {
	

		
		try {
			driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
			takeSnap();
			
			driver.findElementByXPath("//a[@href='/crmsfa/control/createLeadForm']").click();
			takeSnap();
			driver.findElementById("createLeadForm_companyName").sendKeys(CM);
			takeSnap();
			driver.findElementById("createLeadForm_firstName").sendKeys(FN);
			takeSnap();
			driver.findElementById("createLeadForm_lastName").sendKeys(LN);
			takeSnap();
			Select selSource = new Select(driver.findElementById("createLeadForm_dataSourceId"));
			selSource.selectByVisibleText("Conference");
			driver.findElementById("createLeadForm_primaryEmail").sendKeys("TestCreateLead@gmail.com");
			takeSnap();
			driver.findElementByXPath("//input[@value='Create Lead']").click();
			takeSnap();
			String  ViewLeadExpected = "TestFirstName2";
			System.out.println(ViewLeadExpected);
			String ViewLeadActual = driver.findElementById("viewLead_firstName_sp").getText();
			System.out.println(ViewLeadActual);
			if (ViewLeadExpected.equals(ViewLeadActual)){
			System.out.println("CreateLead is verified");
			
			logStep("The test case executed successfully","pass");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logStep("The test case failed","fail");
			e.printStackTrace();
		}
		
}
	
	}



