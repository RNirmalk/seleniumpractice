package testcases.leaftaps;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

public class ProjectMethods extends SuperImplementationClass {

	public String testcaseName, testDec, author, category,sheetname;
	@BeforeSuite
	public void beforeSuite() {
		startReport();
	}
	@AfterSuite
	public void afterSuite() {
		endReport();
	}
	@BeforeClass
	public void beforeClass() {
		initializeTest(testcaseName, testDec, author, category);
	}
	
	
	@BeforeMethod
	public void login() {
		settings();
		driver.get("http://leaftaps.com/opentaps/");
		takeSnap();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		takeSnap();

		driver.findElementById("password").sendKeys("crmsfa");
		takeSnap();

		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		takeSnap();
		driver.findElementByLinkText("CRM/SFA").click();
		takeSnap();
	}
	
	
	
	@DataProvider(name="fetchData")
	public Object[][] data() {
		return ReadExcel.readData(sheetname);
		
	}
	
		@AfterMethod
		public void closeApp() {
			driver.quit();
		}
	}


