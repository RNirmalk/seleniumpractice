package testcases.leaftaps;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SuperImplementationClass extends AdvanceReport implements CommonMethods{
	public RemoteWebDriver driver;
	public int i =1;

	
	@Override
	public void takeSnap() {
		// TODO Auto-generated method stub
		
		File Src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("C:\\NirDocs\\Selenium\\snaps\\img"+i+".png");
		try {
			FileUtils.copyFile(Src, des);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    i++;
		
	}


	@Override
	public void settings() {

		System.setProperty("webdriver.chrome.driver", "C:\\NirDocs\\Selenium\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	}
}
		



