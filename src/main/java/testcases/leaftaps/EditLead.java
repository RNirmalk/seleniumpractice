package testcases.leaftaps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class EditLead extends SuperImplementationClass{

	
		
		
@Test
public void editLeadMethod() throws InterruptedException {
	settings();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		takeSnap();

		driver.findElementById("password").sendKeys("crmsfa");
		takeSnap();

		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		takeSnap();

		driver.findElementByLinkText("CRM/SFA").click();
		takeSnap();
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
		takeSnap();
		driver.findElementByXPath("//a[@href='/crmsfa/control/findLeads']").click();
		takeSnap();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("TestFirstName2");
		takeSnap();
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		takeSnap();
		WebElement FirstNameElement = driver.findElementByXPath("(//a[text()='TestFirstName2'])[1]");
		FirstNameElement.click();
		takeSnap();
		driver.findElementByXPath("//a[text()='Edit']").click();
		
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("CmpNameUpdated");
		takeSnap();
		//String updatedCmpName =  driver.findElementById("updateLeadForm_companyName").getText();
		String updatedCmpName = "CmpNameUpdated";
		System.out.println("Changed Company Name: " + updatedCmpName);
		driver.findElementByXPath("//input[@value='Update']").click();
		takeSnap();
		String ActualUpdatedCmpName1 = driver.findElementByXPath("//span[contains(text(),'CmpNameUpdated')]").getText();
		
		String[] split1 = ActualUpdatedCmpName1.split(" ");
		String ActualUpdatedCmpName = split1[0];
		System.out.println("Actual Company Name:"+ActualUpdatedCmpName);
		
		if(updatedCmpName.equals(ActualUpdatedCmpName))
			System.out.println("Company Name updated successfully");
		else
			System.out.println("NotEqual");
		Thread.sleep(2000);
				driver.quit();
				
}

}
