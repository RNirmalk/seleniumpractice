package testcases.leaftaps;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MergeLead extends ProjectMethods{
	@BeforeTest
	public void initializeCredentials() {
		testcaseName = "Merge Lead";
		testDec = "Merge Lead in leaftaps";
		author = "Nirmal";
		category = "Smoke";
	}
	
	@Test
	public void mergeLead() throws InterruptedException {
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
	driver.findElementByXPath("//a[text()='Merge Leads']").click();
	driver.findElementByXPath("(//input[@id='partyIdFrom']/following::img)[1]").click();
	Set<String> windowHandles = driver.getWindowHandles();
	List<String> Windows = new ArrayList<>();
	Windows.addAll(windowHandles);
	driver.switchTo().window(Windows.get(2));
	driver.findElementByXPath("(//div[@class='x-form-element'])[1]/input").sendKeys("10054");
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	driver.findElementByXPath("//a[text()='10054']").click();
	driver.switchTo().window(Windows.get(0));
	driver.findElementByXPath("(//input[@id='partyIdFrom']/following::img)[2]").click();
	windowHandles= driver.getWindowHandles();
	 Windows = new ArrayList<>();
	Windows.addAll(windowHandles);
	driver.switchTo().window(Windows.get(2));
	driver.findElementByXPath("(//div[@class='x-form-element'])[1]/input").sendKeys("10058");
	driver.findElementByXPath("//button[text()='Find Leads']").click();
	driver.findElementByXPath("//a[text()='10058']").click();
	driver.switchTo().window(Windows.get(0));
    driver.findElementByXPath("//a[text()='Merge']").click();
    driver.switchTo().alert().accept();
    driver.findElementByXPath("//a[text()='Find Leads']").click();
    driver.findElementByXPath("(//div[@class='x-form-element'])[18]/input").sendKeys("10054");
    driver.findElementByXPath("//button[text()='Find Leads']").click();
    String Err = driver.findElementByXPath("//div[text()='No records to display']").getText();
    String ExpectedString= "No records to display";
    if(Err.equals(ExpectedString))
    	System.out.println("Lead1 merged successfully");
	Thread.sleep(10000);
	
		
	}
}
