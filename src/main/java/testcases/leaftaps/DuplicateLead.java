package testcases.leaftaps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DuplicateLead extends SuperImplementationClass {

	
	@Test
	public void DupLead() {
		settings();

		driver.get("http://leaftaps.com/opentaps/");
		takeSnap();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		takeSnap();

		driver.findElementById("password").sendKeys("crmsfa");
		takeSnap();

		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		takeSnap();

		driver.findElementByLinkText("CRM/SFA").click();
		takeSnap();
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
		takeSnap();
		driver.findElementByXPath("//a[@href='/crmsfa/control/findLeads']").click();
		takeSnap();
		driver.findElementByXPath("//span[text()='Email']").click();
		takeSnap();
		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys("TestCreateLead@gmail.com");
		takeSnap();
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		takeSnap();

		WebElement FirstNameElement = driver.findElementByXPath("(//a[text()='TestFirstName2'])[1]");
		String LeadNameCaptured = FirstNameElement.getText();
		System.out.println(LeadNameCaptured);
		FirstNameElement.click();
		driver.findElementByXPath("(//a[text()='Duplicate Lead'])[1]").click();
		takeSnap();

		WebElement Header = driver.findElementByXPath("(//div[text()='Duplicate Lead'])[1]");
		String PageTitle = "Duplicate Lead";
		if (Header.getText().equals("PageTitle"))
			System.out.println(PageTitle + "is verified successfully");
		driver.findElementByXPath("(//input[@value='Create Lead'])[1]").click();
		takeSnap();
		String LeadNameDuplicate = driver.findElementByXPath("(//span[text()='TestFirstName2'])[1]").getText();
		System.out.println(LeadNameDuplicate);
		if (LeadNameCaptured.equals(LeadNameDuplicate))
			System.out.println("Duplicate Lead Name and Captured Lead Name are same!");

	}
	

}
